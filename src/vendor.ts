// Angular 2
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';

// RxJS
import 'rxjs';

// Materialize
import 'materialize-css';
import 'materialize-css/dist/css/materialize.css';


// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...

