import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'gim-retro-panel',
  template: require('./retroPanel.component.html')
})
export class RetroPanelComponent {
}
