import {Component} from '@angular/core';

@Component({
  selector: 'gim-navigation',
  template: require('./navigation.component.html')
})
export class NavigationComponent {
  currentDate: Date = new Date();
}
