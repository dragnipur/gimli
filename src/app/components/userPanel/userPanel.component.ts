import {Component} from '@angular/core';
import {User} from '../../shared/models/user';

@Component({
  selector: 'gim-user-panel',
  directives: [],
  template: require('./userPanel.component.html')
})
export class UserPanelComponent {
  users = [
    new User(1, 'Pieter', 'HBP'),
    new User(1, 'Jack', 'LW'),
    new User(1, 'Evelyn', 'home'),
    new User(1, 'Jeroen', 'free')
  ];
}
