import {Component} from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'gim-velocity-panel',
  template: require('./velocityPanel.component.html'),
})
export class VelocityPanelComponent {

}
