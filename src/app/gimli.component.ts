import {Component} from '@angular/core';
import {UserPanelComponent} from './components/userPanel';
import {VelocityPanelComponent} from './components/velocityPanel';
import {NavigationComponent} from './components/navigation';
import {RetroPanelComponent} from './components/retroPanel';
import '../style/app.scss';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'gimli-app',
  providers: [],
  template: require('./gimli.component.html'),
  directives: [UserPanelComponent, VelocityPanelComponent, NavigationComponent, RetroPanelComponent]
})

export class GimliComponent {
  name: string;

  constructor() {
    name = 'gimli';
  }
}
