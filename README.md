# Gimli

Gimli is an Angular2 / node powered agile gamification dashboard.
### Version
0.0.1

### NPM commands

Gimli uses NPM combined with Webpack for running, testing and packaging:

- npm start (starts the app)
- npm run clean (cleans node_modules, dist and the npm cache)
- npm run clean-install (runs clean and builds the project)
- npm run clean-start (runs clean and builds the project to a local server)
- npm run watch (runs webpack with watch enabled)
- npm run build (builds the project)
- npm run server (bundles the app and runs the webpack-dev-server)
- npm run webdriver-update (updates the webdriver if required)
- npm run webdriver-start (starts the webdriver)
- npm run lint (runs tslint)
- npm run e2e (runs protractor)
- npm run e2e-live (not yet implemented)
- npm run pretest (runs tslint)
- npm run test (runs karma tests)
- npm run test-watch (runs karma tests and watches for changes)
- npm run ci (runs karma tests and e2e tests)
- npm run docs (generates typedoc)
- npm run start (see server)
- npm run postinstall (updates the webdriver and installs the typings)

### Installation

1. npm install
2. npm start
